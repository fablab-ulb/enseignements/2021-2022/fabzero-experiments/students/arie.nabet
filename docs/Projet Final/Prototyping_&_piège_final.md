# I. Objectif final

Je me suis fixé comme objectif final de :

-> Dévelloper un piège non-létal sélectif ciblé sur le frelon asiatique. Ce piège sera basé sur le modèle breton (voir point 3 des modèles proposé en documentation), mais celui-ci sera entièrement fabriqué au FabLab en utilisant les outils à disposition,

-> Basé sur les préférences du frelon et sur documentation, ce piège sera  transparent pour toutes les parois, rouge pour les entrées et les structures, le plus rentable possible, simple à construire, transportable et résistant aux intempéries, 

-> Les mailles des parois seront adaptées aux différents insectes suceptibles de rentrer à pouvoir en ressortir facilement et rapidement pour ne pas être des proies aux frelons, de 7mm d'épaisseur maximum

-> L'appât sera constitué de préférence avec des déchets de rucher, sinon miel + alcool + syrop de cassis,  

-> Trouver des spots à frelons asiatiques sur la [carte de l'observatoire de la biodiversité de Wallonie ](http://observatoire.biodiversite.wallonie.be/enquetes/frelon/) et tester le prototype.

# II. Réalisation du prototype

Les dimensions initiales du prototype ont été établi sur ce qui observable dans la documentation, à savoir autour des 25x25x50cm, pour plus de stabilité j'ai opté pour une largeur plus grande à savoir 25x30x50 cm. Ce sera une boite possèdant deux entrées de chaque coté de forme conique, les parois en plexiglas percées de trous, l'appat sera mis à l'écart de tout insecte mais emballé dans une toile pour laisser les éffluves sortir. Suite à une chute à vélo, j'ai malheureusement fracturé ma main droite, la construction et l'assemblage du prototype doit être encore plus au plus simple!

Tous les documents (portes d'entrée et parois) sont accessibles [ici](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/tree/main/Documents%20projet%20final) sous la license suivante : 

```
FILE   : File Documents Projet Final

AUTHOR : Arié Nabet

DATE   : 2022-06-01


LICENSE : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

```
## A. Portes d'entrées
Les deux portent sont en entonnoir, le bout le plus étroit sera la seule porte d'entrée pour le frelon asiatique pour arriver dans le piège, ces ouvertures doivent faire entre 5 et 7mm de large maximum pour ne empêcher aux plus gros individus de  rentrer (frelons européens, papillons, chauve-souris, etc.). Par chance, un dispositif identique pour ce genre d'entrée a déjà été réalisé sur Thinghiverse, j'ai donc tout simplement imprimé deux fois le [cône](https://www.thingiverse.com/thing:3509859/files) et limé les ouvertures pour avoir la taille souhaitée. 

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/Capture_d_e%CC%81cran_2022-06-14_a%CC%80_16.07.10.png)

Afin de pouvoir avoir un étallonage et une idée de la forme que cette impression pourrait donner en grand, j'ai décidé au préalable d'imprimer la moitié de la taille de l'entrée, de l'insérer dans une boîte de récupération des bobines de fil de PLA et de créer un piège à guêpes sélectif non létal. Test concluant sur le Solbosh, l'appât utilisé était un petit morceau de viande sechée.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/IMG-20220524-WA0002_1_.jpg) ![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/20220506_152353_1_.jpg)

## B. Parois
Il faut en tout 6 parois, 2 x 50x25cm (LATERALES), 2x 50x30cm (SOL + TOIT), 2 x 25x30cm (ENTREES). Les formes sont découpées dans du plexiglas épais de 3mm de tailles de plaques initiales de 1mx50cm environ. J'aurai besoin donc de 0,7 mètre carré pour un prix de 30€ la plaque, ça revient environ à 45€ pour le plexiglas. Ce matériel est inexpliquablement peu/pas disponible à Bruxelles.

SOL : 
![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/Capture_d_e%CC%81cran_2022-06-14_a%CC%80_16.09.06.png)

LATERAL : 
![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/Capture_d_e%CC%81cran_2022-06-14_a%CC%80_16.12.53.png)

TOIT : 
![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/Capture_d_e%CC%81cran_2022-06-14_a%CC%80_16.14.24.png)

ENTREES :
![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/Capture_d_e%CC%81cran_2022-06-14_a%CC%80_16.15.58.png)

Les mailles font 6x12 mm de tailles et sont alignées sur toutes les faces sauf la face inférieure. Des parties de lignes ont été retirées pour pouvoir y insérer des inscriptions. Le dessin a été réalisé par le logiciel Inskape au millimètre près.

La découpe s'est faite via le Lasersaure du FabLab de l'ULB, les parois ont rapidement été découpées et un étallon de la vitesse et puissance était déjà disponible. La vitesse conseillée est de 900 avec une puissance à 90%. Certaines plaques ont demandé un deuxième passage donc surtout ne pas décplacer le plexiglas trop vite après découpe et bien s'assurer que la ventilation est allumée car le plexiglass sent très mauvais lors du passage du laser.

L'angle d'incidence du laser indique dans quel sens la plaque se mettra (côté intérieur et extérieur). La paroi SOL n'a pas de maille mais un vide de 20x10cm centré pour pouvoir y placer le socle d'appat.

## C. Appât et socle d'appât

Il semble le plus interessant de tester le prototype avec des déchets de ruches (miel+cires). En effet ces derniers semblent dans la littérature fortement attirer l'insecte ciblé, 5cl d'alcool vont y être ajouté (vin blanc sucré) pour repousser les abeilles mellifères. La solution de remplacement serait le mélange sirop de cassis-alcool mais est liquide ce qui peut poser problèmes lors des transports. 

Pour la réalisation du socle, le choix du matériel est le plus important : ***léger, résistant aux intempéries, percé de trous mais pas trop grands pour laisser aucun insecte y entrer et démontable (ce sera le seul accès à l'intérieur du piège).*** J'ai donc opté pour une structure en fil métallique fin, recouvert d'une toile de jardin d'une maille inférieure à 1mm. 

## D. Construction et assemblage 
Outils utilisés : colle à plastique, ruban adhésif rouge (isolant des câbles), ruban adhésif en aluminium, équerre-pinces pour coller les plaques en angle droit. Socle d'appat : fil de fer, mètre, toile de jardin

Marche à suivre boite : 

- Coller les entrées aux plaques 30x25, 

- Assembler ces deux plaques avec la plaque du sol et le toît, l'épaisseur (3mm) des plaques vient se coller à même la plaque d'entrée coté 30cm

- Consolider par un tour de ruban adhésif rouge en recouvrant les arrêtes de chaques bordures à l'intérieur et à l'extérieur, les angles droits sont obtenus grâce à l'équerre

- Coller les deux dernières parois restantes, si problèmes à cause d'une découpe en biais, recouvrir d'adhésif en aluminium les espaces donnant libre accès à l'extérieur de la boite

- Consolider avec des tours de ruban adhésif rouge sur toutes les arrêtes du parallépipède rectangle.

- Résultat de la cage :

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/20220611_172730_1_.jpg)

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/20220611_172722_1_.jpg)

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/20220611_172725_1_.jpg)

Marche à suivre socle d'appât : 

-  Réaliser le montage ci-dessous en fil de fer (prévoir 7 x 40cm de tige), il faut assembler d'abord les 2 tiges principales au 5 autres (en respectant les dimensions) puis en les collant via le ruban adhésif en aluminium sur la toile de jardin 

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/Capture_d_e%CC%81cran_2022-06-14_a%CC%80_12.04.32.png)

- Replier les tiges en formes de boite ouverte et rentrer les coins en les plantant sur les tiges des coins adjacents

- Disposer sur une table une toile découpée de 25x15cm, disposer dessus le contenant de l'appât, mettre dessus le montage réaliser en faisant passer le tout par la toile de fond puis ensuite les trous découpés sur la plaque du coté inférieur.  

## E. Liste et prix matériels 

| Nom |  Prix   | 
|-------|-----------|
| Plaque Plexiglas 50x100 cm (x2)    | 30€ x 2      |
| Colle à plastique  | 3,50€ |
| Adhésif rouge   | 3€ (récup) |
| Adhésif métallique   | 4,50€ (récup)  |
|   Fil de fer | 5€ (récup)  |
|   Toile de jardin | 8€ (promo)  |
|   Appât | / €  |
|   Total | 83,50€  |
|   Total hors recup | 71,50€  |


# III. Test 
## Tentatives de captures des frelons à Uccle
J'ai été cherché du miel et de la cire à Gembloux chez un apiculteur, les ruches se situent dans la Province de Namur et n'ont jamais subi d'attaques de frelons. Grâce au site d'observation j'ai été installé le piège Samedi 11 Juin dans un jardin privé non loin de l'observation d'un frelon recensé le 14 mai 2022 au parc du Wolvendael dans la commune de Uccle. 

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/Capture_d_e%CC%81cran_2022-06-10_a%CC%80_15.17.34.png)

Malheureusement, et à mon avis en raison du fait que je n'avais pas encore l'appât adéquat (pas encore de cire à ce moment là) et aucun frelon n'a été attrapé... En revanche d'autres insectes ont été interessés par la cage et ont pu ressortir via les différentes mailles. Une tentative de recommencer l'expérience au sein d'un rucher de Villers-la-ville a échoué suite au hadicap de la main droite et l'incapacité de déplacement.



# IV. Résultats et critiques
Le piège semble sur le papier très bien fonctionner, ce premier prototype semble avoir amélioré les conditions de capture des frelons et n'aurait pas été réalisable sans les outils à disposition du FabLab. L'aspect non-létal est primordiale, aucun insecte n'a été tué lors du test et c'est vraiment une abomination de disposer encore des pièges bouteilles pour au final décimer des centaines d'individus primordiaux à l'écosystème.

La conception en elle-même n'est pas très compliquée mais peut être améliorée via des emboittements ce qui aurait vraiment simplifié la tâche de montage. Il y a une légère erreur de taille sur les parois latérales et un vide de 0,5cm est à remplir. Niveau prix, c'est encore à revoir mais je ne pense pas avoir dépassé les 70€ hors coûts d'utilisation des machines à impressions 3D et de la découpe laser. Le test aurait du se faire sur une période de temps plus grande près d'un rucher et n'est pas du tout révélateur de son efficacité, la saison des piègeages est maintenant donc ça aurait été l'occasion idéale mais manquée. Sur les objectifs fixés de base (point I.), tous sont pratiquement atteints. Les idées pour un prochain prototype sont placées au point suivant, l'utilisation de mon piège reste vraiment interessante pour des organismes de monitoring d'espèces invasives ou pour des apiculteurs touchés par des attaques sur leur rucher.

De plus, au vu de la rapidité d'envahissement de territoire et de propagation de l'espèce en Europe et Amérique, le frelon asiatique pourrait s'installer définitivement dans nos régions. Donc il pourrait s'adapter à nos ecosystèmes avec l'apparition de biocontroles : prédations (oiseaux), parasites (nématodes, champignons, bactéries, virus), parasitoïdes et surtout les mécanismes de défenses observés chez *Apis cerana*, une espèce d'abeille méllifère originaire de la région native du frelon asiatique qui arrive à repousser les attaques des prédateurs. Ces contrôles biologiques sont plus efficace que le piègeage et demande néanmoins une conservation de la biodiversité dans les espaces naturels. 

# V. Futur prototype
- Absolument améliorer le socle d'appât avec une strucutre en PLA imprimée en 3D et pas en fil métallique
- Assemblage via des connections emboitées qui sont découpées au laser et non de la colle pour permettre une facilité de construction et possiblement un pliage de la boite ce qui optimise le rangement.
- Détecteur Arduino pour identifications des vibrations des insectes et alerte de capture du frelon asiatique, possible avec de grandes compétences en codage, chaque insecte possède une fréquence de battement des ailes propre à leur espèce. Cette fréquence peut donc renseigner l'apiculteur de la présence de frelons asiatiques près de son rucher par exemple.
- Trappe pour isoler les frelons des autres insectes 
- Trou d'évacuation des frelons

