## Challenge based learning
- Engage :
      Trouver une solution éfficace aux problemes de prédations du frelons asiatiques en Belgique et protéger les abeilles méllifères.

- Big ideas - Utilisation d'un dispositif de piegeage permettant la capture du frelon asiatique 
- Essential questions :
          - Comment protéger les abeilles?
          - Comment rendre le processus de fabrication simple, low-cost, reproduisible, efficace?
          - Le piège est il létal pour le frelon et comment ne pièger que le frelon asiatique?
          - Comment ne pas nuire à l'activité de la ruche avec mon dispositif?
          - Association avec apiculteurs et Services de la Protection de l'environnement Wallon?

- Challenge - what is your call for action ? J'ai pris connaissance de cette problèmatique cette été, les espèces invasives en général sont un réel problème pour l'environnement et la biodiversité pouvant mettre en péril notre production alimentaire. En effet, la pollinisation par des animaux représente jusqu'à 70% du rendement agricole mondial.
- Investigate - Beaucoup de sources sont disponibles sur Internet, le mieux est quand même d'en discuter avec un apiculteur ou des professionels de la désinsectisation.

- Guiding questions - what are all the questions that needs to be answered in order to tackle the challenge ? Priority ?
      - Tailles des insectes et cycle de vie?
      - Tailles des mailles?
      - Portes d'entrée sélectives?
      - Récolte des frelons?
      - Répartition géographique et disponibilité des frelons pour le test?
      - Application sur le terrain ?
- Guiding activities/ressources - What resources can I use to answer those questions ? Connaissances en Entomologie et Apiculture principallement, le CRA-W, amis apiculteurs
- Analysis and synthesis - write a summary of your findings, facts and data collected : voir point suivant sur la Documentation

## Documentation

Mon projet va donc s'orienter sur les frelons asiatiques, d'un moyen de les pièger sans tuer d'autres insectes et nuire à l'activité de la ruche. C'est donc les objectifs 8 et 15 du projet de dévellopement durable de l'ONU que je vise particulièrement. Le 8 est le "TRAVAIL DÉCENT ET CROISSANCE ÉCONOMIQUE" pour la croissance durable des apiculteurs et le 15, "VIE TERRESTRE" pour la préservation de l'entomofaune.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/Capture_d_e%CC%81cran_2022-06-15_a%CC%80_23.52.27.png)

### A. Formation pour l'éradication du frelon asiatique (01 Juillet 2021)
[Lien Wébinaire Eradication Frelon Asiatique](https://www.cra.wallonie.be/fr/webinaire-sur-le-frelon-asiatique-decembre-2020)

Rencontre avec Pompiers, Apiculteurs, Entrepreneur Parc-Jardin, Désinctiseurs pour recevoir une formation sur la neutralisation du frelon asiatique en Belgique. C’est une opportunité économique pour ceux-ci car de plus en plus demandé, en Mai c’était 100 personnes de formées au sein du laboratoire entomologique du CRA-w à Gembloux. La formation n’est pas accréditée mais en vue de changer via une nouvelle politique sur ce sujet. La neutralisation du nid se fait via une perche télescopique, la manipulation est dangereuse et demande une combinaison plus épaisse que celle de l’apiculteur. La sécurisation des alentours du nid est primordiale surtout en zone urbaine et fait donc recours à la police. En Bretagne on retrouve 15 à 20 nids par km carré et surtout dans les villes. 

Les apiculteurs et le Groupement de Défense des Abeilles font très attention aux frelons asiatiques car ceux ci exercent un stress sur les ruchers par attaques sur les ouvrières revenant chargées de pollen/nectar/eau à la ruche et en empêchant d’autres ouvrières d’en sortir. La production en miel peut être trop petite voir même nulle. Les solutions de cages à mailles empêchant l’accès des frelons a été de faites sous différentes formes mais cela semble aussi stresser les abeilles de par la proximité avec le frelon et l'obstacle que pourrait représenter 

### B. Documentation auprès des chercheurs du CRA-W 
[Lien vers documentation plus complète](http://biodiversite.wallonie.be/fr/le-frelon-asiatique.html?IDC=5999)

- Arrivé en Europe en 2004 et un premier nid observé en Belgique en 2016, le frelon asiatique se retrouve aujourd’hui en quantité de nos jours avec plus de 170 nids observés en 2020 en Wallonie, proches des grandes villes et donc représentant un danger pour la population de par les attaques massives impressionnantes que ces frelons peuvent effectuer en cas de sentiment de menaces proches du nid. Les estimations indiquent avoir pour 2030 une espèce completement instalée et native en Europe de l'Ouest.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/Capture_d_e%CC%81cran_2022-06-01_a%CC%80_15.43.08.png)

- Le frelon asiatique est souvent confondu avec son homologue européen (Vespa crabro) qui légèrement plus grand (3,5 cm contre 3 cm pour V. velutina) et avec une teinte rougeâtre sur le thorax et les pattes, d’autres arthropodes sont susceptible de confusion comme Megascolia maculata (scolie des jardins), Urocerus gigas (sirex géant),...

- La nidification du frelon asiatique ressemble fortement à celle des guêpes sociales. Après l’hivernage (mai-juin), la reine fondatrice construit d’abord un nid primaire proche du sol et à l’abris (préaux, arbre creux, etc.) permettant la ponte du premier couvain et la nymphose des premières ouvrières (parthénogénèse arrhénotoque, reine et ouvrière diploïdes et mâles haploïdes). Celles-ci vont soit renforcer la structure du nid primaire soit le déserter pour construire un nid secondaire bien plus grand et haut perché. La colonie peut devenir très populeuse en fin d’été avec plus de 2.000 individus , c'est donc un insecte super-invasif car une reine fondatrice donne un nid qui donne une population avec un taux de dispersion de 200 à 1.000 et une dispersion sur plusieurs dizaines de kilomètres.

- Un problème majeur de cette espèce est son mode de prédation sur les abeilles et donc un facteur de perte pour les apiculteurs avec un stress permanent des ruches, sans impact sur la production de miel mais un gros impact sur les chances de survie hivernale.

- Afin de détecter un nid, il est important de savoir identifier l'insecte (donc le capturer) et de le suivre jusqu'à son nid, pour cela il doit être en vie et facilement repérable, diverses techniques utilises des plumes colorées attachées à l'abdomen. Les frelons sont ensuites relachés à différents endroits, leurs trajectoires sont mesurées et le nid est trouvable sur une carte par triangulation de différents trajets.

- Le piègeage ne peut avoir lieu que en février, en mai et en novembre pour attraper les fondatrices de nids

- Piège utilisé pour les captures et observations des frelons en Wallonie par le CRA-W : 

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/Capture_d_e%CC%81cran_2022-05-27_a%CC%80_17.56.08.png)

### C. Documentation disponible en ligne

Avant tout, il est important de comprendre le comportement des insectes et des frelons. Les insectes, quand ils sont piègés, tentent toujours de ressortir en se dirigeant vers le haut. Les frelons sont attirés par les abeilles, le miel, les drèches de ruches, les fleurs de camélia et de lierre, la couleur rouge et l'alcool/vinaigre principalement mais sont aussi attirés par les phéromones laissés par leurs congénaires et la présence d'autres frelons lorsque ceux-ci sont en contact avec des proies et de la nourriture. Par contre, ils n'aiment pas l'obscurité et la couleur noire. Si l'on utilise tout autre appât que miel et cires, un toit translucide (plexiglas ou vitrage) est essentiel pour augmenter la température à l'intérieur du piège et de fait la quantité d'effluves. Le piège doit être sélectif pour éviter de tuer inutilement la faune environnente : 

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/FB_IMG_1654363757577_1_.jpg)

Tailles des insectes suceptibles d'être piègés à coté des ruches :

| Nom |  Orde phylogénétique    |  Tailles au thorax  |       Ciblés     | Photos|
|-------|-----------|------------|----------------------|----|
| Abeilles méllifères (sauf reine)    | Hyménoptères      |  H :3-4mm , L : 1,1-1,3cm       |    Non              ||
| Guêpes communes  | Hyménoptères |  H : 3-4mm, L: 1,5cm| Non   ||
| Frelons asiatiques   | Hyménoptères  |  H : 5-8mm, L : 2cm | Oui   ||
| Frelons européens   | Hyménoptères  |  H : 7-10mm, L : 3cm  | Non   ||
|   Syrphides | Diptères  |  très variables | Non   ||


1. **Le Piège Low-Cost** proposé dans toutes les recherches avec différents modèles : 

      - *Matériels* : Bouteilles en plastique (2L), Corde, Appâts : alcool + sucre (bières, vin blanc, sirop de cassis,...) 
      - *Outils* : chalumeau, aiguille, tige métallique, aggrafeuse, paire de ciseaux, cutter
      - *Temps de fabrication* : 1 heure
      - *Appât* : Alcool devrait faire fuire les abeilles et attirer le frelon asiatique, sucre pas néccéssaire car le frelon est carnivore 
      - **Avantages** : vraiment le plus simple, prix de frabication le plus bas (*10€*)
      - **Inconvénients** : D'après une étude sur les régions impactées par le frelon asiatique, ce genre de pièges attrapent et tuent aussi le reste de l'entomofaune, l'appât n'étant pas sélectif et létal par noyade + ébriété mais le plus critique reste le ratio frelon mort/insectes morts qui s'élève à 1 pour 100 dont des espèces en voie de disparition ou utile au service de pollinisation
      - Exemple en cliquant sur ce [lien YouTube](https://www.youtube.com/watch?v=GVvKpJdjYbc&ab_channel=LaR%C3%A9publiquedesPyr%C3%A9n%C3%A9es)

2. **Pièges Low-Cost en impression 3D**, idem que le précédent mais modélisés en 3D
      - [Exemples](https://www.thingiverse.com/thing:2989803): 

3. **Piège sélectif apiculteur breton**, piège primé inventé par un apiculteur qui s'est vu 35 ruches disparaître à cause du frelon asiatique, il commercialise ses pièges en France, Belgique et Espagne. Le principe est de pièger le frelon dans une boîte avec des mailles plus petites que lui, il ne peut donc que rentrer par le réducteur d'entrée attiré par l'appât : une planche de miel sortie d'une ruche. Le piège peut s'empiler sur d'autres et se place à côté des ruches
      - *Matériels* : Réducteurs d'entrée calculer au dixième de millimètre avec cache, Caisse en plastique + couvercle + grillage, bac à appât, appâts : planche de rucher contennant du miel 
      - *Outils* : divers, souvent retrouvé chez quelqu'un qui possède des ruches
      - *Temps de fabrication* : 2h-4h 
      - *Appât* : Miel est censé attirer le frelon et alcool repousse les abeilles 
      - **Avantages** : système sélectif capturant uniquement le frelon asiatique basé sur le comportement des insectes, ce piège est non-létal (sauf si on laisse enfermé les insectes)
      - **Inconvénients** : Le principal inconvénient est que le matériel est en rupture de stock pour les réducteurs d'entrée et conception, le marché dépend de ces portes qui sont produites en France par une entreprise et le même concept a été utilisé par l'enseigne *Ruche et Ruchette de France* (voir point 4). Le piège semble pouvoir accueillir une trentaine de frelons, ce serait peut être du aux parois non transparentes qui attirent les frelons sur les bords extérieurs et pas vers les entrées. Enfin, l'appât peut attirer d'autres organismes comme des parasites d'abeilles qui pourront ressortir et contaminer les ruches
      - [Exemples](https://www.youtube.com/watch?v=_OIyyxRC7Ro&t=22s&ab_channel=LeParisien)
4. **Pièges sélectifs commercialisés**
      - [Site d'achat "Ruche et ruchettes de France](https://ruches-et-ruchettes-de-france.com/fr/), prix d'achat à 55€ en précommande, le piège et nouveau modèle RedTrap5, permet de capturer une centaine de frelons asiatiques uniquement en deux semaines. Il est préconisé d'en ajouter 4 de plus pour en cas de grosse pression du prédateur des ruchers.
      - [Exemple de piège à placer devant ses ruches :](https://www.apiculture.net/recolte/5366-trappe-a-pollen-dentree-anti-frelons-quarti.html) , son avantage est vraiment son prix (25€ hors livraison par ruche) mais il a été remarqué que disposer un obstacle devant une ruche nuit fortement aux passages des abeilles ouvrières. Le fait aussi qu'il y ait des frelons emprisonnés devant l'entrée pourrait aussi représenter un stress pour la ruche.  

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/Capture_d_e%CC%81cran_2022-03-08_a%CC%80_13.30.03.png)
      - D'autres pièges sont disponibles sur le web, la gamme de prix ne dépasse pas les 100€, les mêmes principes sont utilisés avec par moment d'autres applications comme des brosses à placer en entrée de ruche pour récupérer le pollen

5. **Piège fait-maison non-létal et sélectif**, ces derniers sont inspirés des pièges commercialisés, les formes et tailles sont très variables et utilise beaucoup de matériel d'apiculture comme des grillages à reine (epaisseur des mailles de 4mm). Ces pièges "fait-maisons" ont une efficacité réduite du au fait que le matériel n'est pas accessible à tous, difficile à réaliser pour les moins bricoleurs et l'outillage dangereux à l'utilisation (scie circulaire, perceuse, chalumeau, soudure,...). Beaucoup de créations sont adaptées pour les entrées de ruches mais celles-ci représentent quand même un risque pour les abeilles qui se retrouvent avec des frelons piègés à l'entrée de la ruche. Une copie de ce genre de pièges existe aussi sous la forme imprimée en 3D mais demande beaucoup de PLA : 

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/Capture_d_e%CC%81cran_2022-06-14_a%CC%80_16.06.23.png)

6. Recap 

| Piège |  Letal    |  Selectif  |       Efficacité     | Disponibilité matériel | Transparent | Stress abeilles  | Prix | Lien | 
|-------|-----------|------------|----------------------|------------------------|------------------|------|----|----|
| Piège bouteille     | Oui      |  Non      |    1%                | Oui | Oui                    | Non   | 1-5€   |[Lien YouTube](https://www.youtube.com/watch?v=GVvKpJdjYbc&ab_channel=LaR%C3%A9publiquedesPyr%C3%A9n%C3%A9es) |
| Piège bouteille 3D  | Oui|  Non| 1%   |    Oui (si possession d'imprimante 3D)| Oui | Non  | 10€ | [Exemple](https://www.thingiverse.com/thing:2989803)|
| Piège breton   | Non  |  Oui| Inconnue   |   Non | Non     | Non | 150€ | [Lien YouTube](https://www.youtube.com/watch?v=_OIyyxRC7Ro&t=22s&ab_channel=LeParisien) |
| Piège breton commercial   | Non  |  Oui | 10%   |    Oui    | Oui et non | Non | 35-140€ (hors livraison)|[Exemple](https://www.jabeprode.fr/fr/boutique/passer-une-commande-1), [Redtrap](https://www.icko-apiculture.com/piege-a-frelon-asiatique-red-trap.html?___store=fr&nosto=nosto-page-category1)|
|   Piège fait main | Non  |  Oui et non | Inconnue   |  Oui      | Non | Oui et non | 75-100€ hors matériel de construction| / |

### D. Exemple d'entreprise responsable de fabrication de piège : JABERPRODE 
![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/Capture_d_e%CC%81cran_2022-06-01_a%CC%80_16.18.17.png)
![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/0788678b01d0c632d4eb23137f66411b8250ee4d/docs/images/Capture_d_e%CC%81cran_2022-06-01_a%CC%80_16.18.44.png)
### E. Règlementations et conseils d'utilisation 
Suite à ces recherches, je me suis rendu compte qu'il y avait des périodes de piègeage autorisé en Belgique et en France. De plus afin de protéger la faune attirée par les appâts utilisés doivent être constitués de cires mielleuses (Brèches ou opercules des récoltes ou cadres de miel brisés menu) qui doivent impérativement être protégés par un grillage fin (fixé au fond, ce grillage fin utilisé peut être celui utilisé pour l’aération des planchers de ruches ou du tapis à propolis souple pour les apiculteurs) pour interdire l’accès à tous les insectes. Quatre raisons essentielles à cela :

- Une préservation de l’appât durant toute la saison pour ne pas avoir à le remplacer et empecher de nourrir les insectes piègés.

- La nécessité d’une sortie rapide des insectes non cible pour éviter qu’ils ne deviennent les proies des frelons asiatiques déjà présents 

- Le risque potentiel de transmission de pathologies par la contamination des abeilles par des spores de champignons présent dans les cires toxiques pour elles 

- La nécessité de ne pas nourrir le frelon pour que sa mort intervienne au plus vite. Même un insecte nuisible ne doit pas souffrir inutilement comme dans bon nombre de pièges "conventionnels et artisanaux". 

- Un piegeage est conseillé durant 3 semaines deux fois par an, de mai à juin et en novembre afin de pièger les reines fondatrices

- Chaque capture doit être signalée aux pompiers, une carte des observations des frelons asiatiques et de l'éradication des nids pour la Région wallone est disponible [en ligne](http://observatoire.biodiversite.wallonie.be/enquetes/frelon/).
