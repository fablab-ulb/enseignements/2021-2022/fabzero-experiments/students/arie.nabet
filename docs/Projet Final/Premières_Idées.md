## Premières idées face à des problèmatiques

### 1. Dévellopement d'un système de piégeage à frelons asiatiques 

Durant mon stage conventionné de longue durée en entomologie, j'ai pu approcher divers sujets dont le problèmes des apiculteurs face à l'aggressivité du frelon asiastique, _Vespa velutina_, sur leurs ruchers. Les attaques des frelons se font lorsque l'abeille ouvrière revient à la ruche chargée de pollen ce qui a pour effet de stresser la ruche tout entière diminuant le taux de survie hivernal du rucher. Plusieurs idées ont déjà été dévellopées jouant sur la taille du frelon, et grâce à l'imprimerie 3D, j'aimerai pouvoir reproduire voir améliorer un de ces modèles existants sur des ruches conventionnelles. Certaines failles sont probables dû au facteur biologique méconnu de la ruche, des pertubations de l'environnement peuvent aussi entraîner des pertes de rendement. 

[Exemple de piège :](https://www.apiculture.net/recolte/5366-trappe-a-pollen-dentree-anti-frelons-quarti.html) 

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/Capture_d_e%CC%81cran_2022-03-08_a%CC%80_13.30.03.png)

### 2. Fabrication d'une paille filtrante 

La conception d'un projet pour ce cours doit en principe répondre à une problematique soulevées dans les [**17 Objectifs de Développement Durable de l'ONU**](https://www.un.org/fr/exhibit/odd-17-objectifs-pour-transformer-notre-monde). L'utilité du FabLab au niveau mondial est intéressant pour sa facilité d'échange pour reproduire à l'identique un même concept de l'autre côté du globe. Le 6e objectif de l'ONU est le suivant : "*Garantir l’accès de tous à des services d’alimentation en eau et d’assainissement gérés de façon durable*". C'est pourquoi je m'intéresse vivement à la fabrication d'un systême de filtration incorporable dans une paille et possiblement dans un robinet.
 

### 3. Fabrication d'une poubelle flottante afin de récupérer des déchets de surfaces 

Un énorme soucis que j'ai dans la vie, c'est de m'attrister face à la masse de déchets d'origine anthropique présents dans les écosystèmes urbanisés que je traverse à Bruxelles. La pollution plastique est un réel problème qui demande encore beauucoup d'innovations car présente à toute les échelles (micro- et macroscopique) avec différentes compositions moléculaires. C'est pourquoi je m'intéresse uniquement aux plastiques de toutes tailles mais présents à la surface de l'eau dans un cours d'eau calme (comme le canal de Bruxelles) ou un étang, lac etc. . Le dispositif final serait donc flottant, muni d'un bac de récupération et motorisé.    

## Choix de la problèmatique 

Parmis les 3 idées proposeées , le piege à frelons fut l'épreuve la plus attirante pour la réalisation de mon projet final au FabLab de l'ULB. Cette problèmatique est assez complexe de par le facteur biologique nouveau causé par l'invasion progressive de l'Europe de l'Ouest par cette espèce prédatrice des abeilles méllifères. Différentes solutions ont déjà été abordées, certaines ont été commercialisées et d'autres sont expliquées en capsules vidéos et publiées sur Internet. Ce sera donc un projet basé sur les idées préexistentes mais fabriqué entièrement au FabLab et disponible à n'importe quel personne intéressée.
