# Présentation 

Bonjour, bienvenue sur ma page personnelle pour ma réalisation de projet au sein du FabLab de l'Université Libre de Bruxelles pour le cours **"How to Make Almost Any Experiments"** . Cette page est éditée par [Gitlab](http://gitlab.fabcloud.org). Le logiciel utilisé transforme de simples fichiers texte
écrit au format [Markdown](https://en.wikipedia.org/wiki/Markdown), dans ce site navigable.
Ce cours a pour objectif d’immerger des étudiants de différentes facultés à la manipulation des outils de fabrication numérique. Les outils appris seront mis en pratique à travers un projet de notre choix basé sur l’un des 17 objetifs du développement durable de l’ONU.

## A propos de moi 

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/3bd68d2a6e1a11be5e5b8b7d6a98d3c60ee34228/docs/images/20220224_115626.jpg)

* Nom et prénom : Nabet Arié
* Âge : 23 ans, né le 23 décembre 1998
* Etudes et formations : MA2 en Bioingénieur en sciences du vivant et sciences agronomiques
* Centre d'intérêts : Les voyages, les amis et la protection de la biodiversité aquatique et terrestre 

## Parcours

Je suis né à Bruxelles, je n'ai jamais déménagé de cette ville et je m'y plais énormément. A la fin de mes études secondaires (Athénée Royal Jean Absil) en 2016, je ne savais toujours pas quoi faire comme étude/travail mais j'étais déjà sensibilisé aux problèmes de climat et de pertes en biodiversité. J'ai donc décidé de découvrir différents métiers (archéologie, illustrateurs et ingénieurs) et c'est la partie interfacultaire de Bioingénieur qui m'a le plus attiré. En effet, pouvoir étudier les sciences du vivant en même temps que comprendre des systèmes complexes physiques est une opportunité pour un apprentissage large donnant accès à des professions très mixtes. J'ai donc commencé mes études en 2016 au sein de L'Ecole Interfacultaire de Bioingénieurs à l'ULB, la première année ne fut pas concluante mais la volonté de réussir pris le dessus par la suite. C'est ainsi que j'ai commencé mon Master en Agronomie en 2020 accompagné de 5 autres étudiants tout aussi motivés. Grâce à ces études, nous avons pu réaliser différents stages en entreprises et à la ferme expérimentale du CARAH (Centre de Recherche en Agronomie en province du Hainaut) nous rapprochant de certaines réalités sur la production alimantaire absentes des discours urbains. 

### MFE

Grâce au Laboratoire d'Agroécologie de l'ULB, il m'est possible de réaliser mon Mémoire de Fin d'Etude sur l'étude de la pollinisation par les abeilles sauvages aux Îles Canaries. L'analyse se porte essentiellement sur la biogéographie des insectes et des plantes-hôtes, ma présence sur le terrain sera essentielle pour la capture, l'épinglage et  l'identification de plus de 125 espèces d'abeilles de la région.  

## Travaux réalisés par le passé

Lors de mon bachelier il m'a été possible de participer à Ecotrophelia, _Food At Work Award_ , un concours inter-universitaire pour ​développer des produits alimentaires innovants, commercialisables et durables. Les concours à l’innovation sont pour les étudiants une occasion unique pour découvrir en pratique les opportunités d’un job dans l’industrie alimentaire mais aussi sur la gestion d'équipe et les aspects commerciaux. Pour ce projet, moi et mon groupe avons présenté des croquettes de légumes oubliés (topinambours, panais, choux raves, radis noirs) dont la chapelure provient de pains invendus. 

En été 2021, j'ai pu intégré durant 3 mois le laboratoire de recherches entomologiques en Santé des Plantes et des Forêts au sein du Centre de Recherche en Agronomie de Wallonie (CRA-w). Ce stage fut ma meilleure expérience professionelle,le personel de laboratoire était réparti en fonction des différents Ordres phylogéniques des insectes à étudier ce qui a permis des approfondissements dans plusieurs grandes familles avec différents protocoles de piègeage et d'élevage jusqu'à l'expérimentation de produits phytosanitaires (insecticides ou juste des extraits naturels) sur certains ravageurs de Belgique.

Ci-dessous, une photo de moi dans le verger de conservation du CRA-w contenant plus de 5.000 variétés de pommes, poires et autres arbres fruitiers de nos régions. La pollinisation des fleurs est assurée par un rucher d'abeilles domestiques et sauvages.

![This is the image caption](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/6501d566ba2e488e3fded7ad3c239a8ae0880c98/docs/images/IMG_20210816_132456.jpg)

