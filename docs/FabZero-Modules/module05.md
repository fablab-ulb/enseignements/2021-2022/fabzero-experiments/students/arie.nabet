# 5. Electronique 1 - Prototypage

Pour ce 5e module, c'est la plateforme de prototypage open-source Arduino qui nous est enseigné. Celle-ci permet aux utilisateurs de créer des objets électroniques interactifs à partir de cartes électroniques matériellement libres sur lesquelles se trouve un microcontrôleur. Les schémas de ces cartes électroniques sont publiés en licence libre (sauf les microcontrôleur).

Le microcontrôleur peut être programmé pour analyser et produire des signaux électriques, de manière à effectuer des tâches très diverses comme le pilotage d'un robot, de l'informatique embarquée, des détections,...

Arduino peut être utilisé pour construire des objets interactifs indépendants (prototypage rapide), ou bien peut être connecté à un ordinateur pour communiquer avec ses logiciels.

## Documentation et matériels
- La première chose à faire (en plus de se procurer le matériel nécéssaire) est de télécharger le logiciel [Arduino IDE](https://www.arduino.cc/en/software) et d'y associer l'ARDUINO UNO (dans ce cas) via le port USB via la fenêtre suivante dans le menu affiché : ![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/Capture_d_e%CC%81cran_2022-04-12_a%CC%80_18.21.45.png)
- Les modules annexes (comme le capteur à ultrason) nécessite l'installation d'une bibliothèque supplémentaire. Le téléchargement se fait par un fichier .zip dans la fenêtre du menu. ![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/Capture_d_e%CC%81cran_2022-04-12_a%CC%80_18.21.11.png)

## Réalisation de montages électroniques
### 1. Montage sans code : test des ampoules 
Permet de créer un courant simple et donc te tester les ampoules et de voir quel branche de l'ampoule est l'entrée ou la sortie.
- Matériels : Une lampe LED, une résistance 220 Ohm, un Arduino hardware et deux câbles de branchement
- Montage : Le courant part de la sortie 5V, relier à la resistance puis à l'ampoule et enfin à la terre (sortie GND)
![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/20220412_182831_1_.jpg)

### 2. Montage basique d'exemple : Blink 
- Matériels : Une lampe LED, une résistance 220 Ohm, un Arduino hardware et deux câbles de branchement
- Montage : l'entrée du courant se fait par le pin 13
- [Lien vidéo YouTube de mon montage](https://www.youtube.com/shorts/U-vI-xPLlFg)
- Code extrait du logiciel:
```
/*
  Blink

  Turns an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the UNO, MEGA and ZERO
  it is attached to digital pin 13, on MKR1000 on pin 6. LED_BUILTIN is set to
  the correct LED pin independent of which board is used.
  If you want to know what pin the on-board LED is connected to on your Arduino
  model, check the Technical Specs of your board at:
  https://www.arduino.cc/en/Main/Products

  modified 8 May 2014
  by Scott Fitzgerald
  modified 2 Sep 2016
  by Arturo Guadalupi
  modified 8 Sep 2016
  by Colby Newman

  This example code is in the public domain.

  https://www.arduino.cc/en/Tutorial/BuiltInExamples/Blink
*/

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```
### 3. Montage : Capteur à ultrasons HC-SR04
- Matériels : un Arduino hardware, un capteur à ultrasons HC-SR04, trois câbles de branchement et la bibliothèque "SR04.zip" à télécharger au préalable
- Explications du capteur : Le capteur HC-SR04 est un capteur à ultrason low cost, c'est à dire qu'il émet un vibration sonore non perceptible par nos oreilles et le recepte une fois que le signal sonore aie rebondi sur un obstacle, le temps entre l'emission et la réception de l'ultrason indique la distance entre l'émetteur et l'obstacle. Ce capteur fonctionne avec une tension d'alimentation de 5 volts, dispose d'un angle de mesure de 15° environ et permet de faire des mesures de distance entre 2 centimètres et 4 mètres avec une précision de 3mm (en théorie, dans la pratique ce n'est pas tout à fait exact).
Voici un schéma du signal qui opère aux entrées et sorties du capteur : ![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/sonar_signal.jpeg)
- Montage : Placer le SR04 sur la board, 4 possibilités de branchements avec le hardware : connection du trigger avec le pin12, de l'echo avec le pin 11, le vcc avec la sortie 5V et le Gnd avec la terre. 
- [Lien vidéo YouTube de mon montage](https://www.youtube.com/shorts/HUcV-RPgGyw)
- Code extrait du logiciel:
```
#include "SR04.h"
#define TRIG_PIN 12
#define ECHO_PIN 11
SR04 sr04 = SR04(ECHO_PIN,TRIG_PIN);
long a;

void setup() {
   Serial.begin(9600);
   delay(1000);
}

void loop() {
   a=sr04.Distance();
   Serial.print(a);
   Serial.println("cm");
   delay(1000);
}
```
### 4. Montage : Détecteur de mouvement et de position par signal lumineux 
- Objectifs : Réaliser un détecteur de mouvement/distance d'un objet 
- Matériels :un Arduino hardware, un capteur à ultrasons HC-SR04, 13 câbles de branchement, 7 résistances de 220Ohm, 7 ampoules LEDs et la bibliothèque "SR04.zip" à télécharger au préalable
- Montage : même branchement que pour le point 3., on relie la première ampoule à l'entrée D2 et le reste des ampoules dans l'ordre A5->A0 où A0 représente la dernière ampoule (celle qui s'éteindra en premier)
- [Lien vidéo YouTube de mon montage](https://www.youtube.com/shorts/Y26IZ-yNo8g)
- Code :
```
//détecteur de mouvement signal lumineux 

#include "SR04.h"
#define TRIG_PIN 12
#define ECHO_PIN 11

const int LED1 = A0;
const int LED2 = A1;
const int LED3 = A2;
const int LED4 = A3;
const int LED5 = A4;
const int LED6 = A5;
const int LED7 = 2;

int duration = 0;
int distance = 0;

void setup() 
{
  pinMode(TRIG_PIN , OUTPUT);
  pinMode(ECHO_PIN , INPUT);
  
  pinMode(LED1 , OUTPUT);
  pinMode(LED2 , OUTPUT);
  pinMode(LED3 , OUTPUT);
  pinMode(LED4 , OUTPUT);
  pinMode(LED5 , OUTPUT);
  pinMode(LED6 , OUTPUT);
  pinMode(LED7 , OUTPUT);
  
  Serial.begin(9600);

}

void loop()
{
 digitalWrite(TRIG_PIN, LOW);
  delayMicroseconds(2);
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);  
  duration = pulseIn(ECHO_PIN, HIGH);
  distance = duration/58.2;

  if ( distance <= 5 )
  {
    digitalWrite(LED1, HIGH);
  }
  else
  {
    digitalWrite(LED1, LOW);
  }
  if ( distance <= 10 )
  {
    digitalWrite(LED2, HIGH);
  }
  else
  {
    digitalWrite(LED2, LOW);
  }
  if ( distance <= 15 )
  {
    digitalWrite(LED3, HIGH);
  }
  else
  {
    digitalWrite(LED3, LOW);
  }
  if ( distance <= 20)
  {
    digitalWrite(LED4, HIGH);
  }
  else
  {
    digitalWrite(LED4, LOW);
  }
  if ( distance <= 25 )
  {
    digitalWrite(LED5, HIGH);
  }
  else
  {
    digitalWrite(LED5, LOW);
  }
  if ( distance <= 30 )
  {
    digitalWrite(LED6, HIGH);
  }
  else
  {
    digitalWrite(LED6, LOW);
  }
  if ( distance <= 35 )
  {
    digitalWrite(LED7, HIGH);
  }
  else
  {
    digitalWrite(LED7, LOW);
  }
  delay(100);
}
```
