# 3. Impression 3D

Cette semaine, nous avons eu droit à une présentation sur l'utilité de l'impression 3D en pharmacologie ainsi qu'une introduction sur la manipulation d'imprimante 3D afin de développer les modules créer lors du Module 2 de ce cours. 

## Documentation et Logiciels

Les imprimantes 3D disponibles au FabLab sont de la marque Prusa, elles seront utilisées pour réaliser notre kit de FlexLinks modélisé au Module 2. Le logiciel à installer sur ordinateur est le [PrusaSlicer 2.4.0](https://www.prusa3d.com/page/prusaslicer_424/?gclid=CjwKCAjwo8-SBhAlEiwAopc9W0GpiLk7zHYMAgLKpdjGBDmtMD0G-yRtAJreLIEiUoj1EV4qm_DYexoCxL4QAvD_BwE). Il est alors possible de _uploader_ les fichiers provenant du logiciel de modélisation (OpenScad dans mon cas). 

Quelques mesures sont à prendre en compte avant toute impression : 

- Remplissage des formes : 15% pour tout objet décoratif, 25-30% pour un objet solide, les [motifs de remplissages](https://all3dp.com/fr/2/cura-infill-remplissage-type/) sont multiples et permettent des propriétés différentes pour un même matériel (élasticité, flexibilité, resistance aux chocs, réduction du temps d'impression)

- Jupe, bordure et support : la jupe est la délimitation, le périmètre, dans lequel va s'inscrire l'objet sur la plateforme (2 à 3mm d'espace entre l'objet et la jupe); la bordure est nécessaire pour les objets imprimés en hauteur afin de les maintenir droit lors de l'impression, plus l'objet est haut, plus la bordure doit être grande; les supports peuvent être ajoutés pour maintenir une structure sur un angle

- Taille et hauteur de la buse : la taille de la buse influence la taille d'une couche de plastique, la hauteur de couche est la hauteur de la buse divisé par deux 

- Température du PLA (Acide Polylactique) : 215°C dans la buse, 60°C sur le plateau

- Callibration de l'imprimante (après chaque changement de plastique, de buse,...) : Première couche pour détecter le plateau, à nettoyer avec de l'éthanol, un _torture test_ peut servir à déterminer l'état de l'inclinaison dans le vide et autres contraintes que le thermoplastique pourrait subir lors de l'impression

- Le transfert de fichier entre le logiciel et l'imprimante 3D se fait via une clé USB

## Importation d'un fichier .stl à partir de OPENSCAD 
Sur openscad3D, exporter le fichier à imprimer vers un format lisible par le logiciel PrusaSlicer comme sur l'image qui suit :
![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/Capture_d_e%CC%81cran_2022-04-19_a%CC%80_15.47.23.png)
## Utilisation du logiciel et impression 
Voici les paramètres utilisés pour l'impression du FlexStick 

![](../images/Module3-4.jpg)

![](../images/Module3-3.jpg)

![](../images/Module3-2.jpg)

![](../images/Module3-1.jpg)

Il faut ensuite cliquer sur "Exporter le G-code", le copier sur une carte SD au format .gcode et la connecter à l'imprimante 3D.

## Impression de la pièce
Le premier essai ne fut pas très concluant, j'ai oublié de mettre les supports... 

Un deuxième test fut effectué avec en plus une pièce porte clé tout droit sorti du site [Thingiverse](https://www.thingiverse.com/thing:1276095/files) où le fichier .stl est directement disponible. 
