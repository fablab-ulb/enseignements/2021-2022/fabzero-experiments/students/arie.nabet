# 4. Découpe assistée par ordinateur

Cette semaine, nous avons pu approcher et utiliser différentes découpeuses au lazer en créant nos propres formes via le logiciel [Inskape](https://inkscape.org/release/inkscape-dev/?latest=1).

## Présentation des consignes de sécurité 

Pour éviter tout risque d'incendie, il est indispensable de connaître avec certitude quel matériau est découpé, de toujours activer l’air comprimé, allumer l’extracteur de fumée, savoir où est le bouton d’arrêt d’urgence, savoir où trouver un extincteur au CO2 et toujours rester à proximité de la machine jusqu'à la fin de la découpe.
Et pour éviter des dégats sur la santé, il est recommandé de ne pas regarder fixement l'impact du faisceau LASER et de ne pas ouvrir la machine tant qu'il y a de la fumée à l'intérieur après une découpe. 

## Documentation et spécifications des machines de découpe 

Il existe une liste de matériaux disponibles à la découpe (Papier, cartons, textiles, plexiglass, bois contreplaqué,...), certains sont déconseillés (métaux, PE, PET, PP) et d'autres sont carrément interdits (PVC, Cuivre, Téflon, cuir,...).
La découpe d'une image peut aller beaucoup plus vite si l'image est vectorisée, cela est possible via des [logiciels](https://fr.vectormagic.com/) gratuits sur internet. Les machines sont équipées d'une caméra à l'intérieur pour fixer le modèle sur une zone de découpe. 

### Spécifications Epilog Fusion Pro 32

- Surface de découpe : 81 x 50 cm
- Hauteur maximum : 31 cm
- Puissance du LASER : 60 W
- Type de LASER : Tube CO2 (infrarouge)
- Découpe la plus fine : 0,01mm

### Spécifications Lasersaur

- Surface de découpe : 122 x 61 cm
- Hauteur maximum : 12 cm
- Vitesse maximum : 6000 mm/min
- Puissance du LASER : 100 W
- Type de LASER : Tube CO2 (infrarouge)

### Spécifications Full Spectrum Muse

- Surface de découpe : 50 x 30 cm
- Hauteur maximum : 6 cm
- Puissance du LASER : 40 W
- Type de LASER : Tube CO2 (infrarouge) + pointeur rouge
- Connexion par WIFI

## Réalisation d'un étalonnage sur matériel inconnu (Full Spectrum Muse)

### Première découpe

Selon le matériel et le niveau de découpe (gravure/coupe) que vous utilisez, vous devez sélectionner la bonne vitesse et la bonne puissance de découpage au laser. La vitesse élevée signifie que le laser passera rapidement sur le matériau afin d'avoir moins d'effet sur celui-ci (brûler, fondre, mouler, graver). Une vitesse de 75 à 80 % est recommandée pour des lignes plus propres. Une vitesse lente et une puissance élevée coupent des matériaux plus épais. Augmenter la puissance ou diminuer la vitesse ont plus ou moins le même effet : marquer plus profondément. 

Pour connaître la vitesse et la puissance dont vous aurez besoin pour couper votre matériau, il est indispensable de tester plusieurs combinaisons sous forme de tableau. Vous pouvez faire varier la vitesse horizontalement et la puissance verticalement. Le but est de trouver une vitesse plutôt élevée et la puissance en conséquence.

Lors de notre premier approche en groupe, nous avons pu tester différentes vitesses et puissances sur du carton, grâce au logiciel Inskape il est possible de paramètrer différents mode de lazer sur une même image/dessin en selectionnant les courbes distinctes pour le test. 
Ci-dessous nous retrouvons les différents modèles de tests ainsi que le résultats après la découpe : 

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/folding_model.jpg)
![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/speedpower.jpg)
![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/star_strip.jpg)

### Emboittement des pièces
On peut donc modifier les valeurs de vitesse et de puissance en fonction de tous les matériels disponibles.
Il est possible de découper une pièce d'étallonage de l'imbrication pour régler les paramètres en fonction du trait de scie du laser (son rayon). Le test s'éffectue en enboitant les pièces d'étalonnage jusqu'à trouvé deux mesures adéquates. Elles seront ensuite ajoutée aux mesures lors de la découpe.
Pour connaître le trait de scie de votre laser, il faut  découper l'échelle ci-dessous avec le matériel choisi.
![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/kerf.jpg)

## Kirigami 
Pour les travaux pratiques nous avons pu tester les différentes machines de découpe. J'ai pu travailler sur la gravure mais aussi sur le kirigami (l'art du découpage du papier, et l'ensemble des œuvres en papier découpé qui en sont issues). J'ai donc réalisé une maquette avec pour les traits rouges les paramètres suivants : vitesse = 75, puissance = 50 et traits noirs : vitesse = 75, puissance = 90 sur le Lazersaur. Les dimensions du cube sont respectées mais le papier-carton utilisé n'est pas adéquats pour ce genre de pli donc il faudra le refaire.

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/Capture_d_e%CC%81cran_2022-04-12_a%CC%80_10.07.33.png)

![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/20220412_101818_1_.jpg)
