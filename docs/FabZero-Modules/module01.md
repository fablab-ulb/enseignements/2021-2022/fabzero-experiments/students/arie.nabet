# 1. Documentation

Pour le premier cours d'introduction, une approche par la documentation fut néccésaire pour l'édition de cette page via GitLab. Afin de pouvoir relier cette page-web à mon ordinateur, une clé de type SSH fut créée et connectée à la page principale d'édition. 

## Installation de Git
Etant donné que je travaille sur MacOS, la fonctionnalité Git est prévue de base dans le terminal de l'ordinateur. Une vérification de la version installée peut s'obtenir via la commande : 
```
git --version
```
Le terminal confirme en affichant la version actuelle utilisée, à savoir : 
```
git version 2.35.1 
```
## Configuration de Git
La configuration de Git s'obtient en écrivant dans le terminal le code ci-dessous, en remplaçant avec mon nom d’utilisateur et mon adresse mail.
```
git config --global user.name "your_username"
git config --global user.email "your_email_address@example.com"
```
## Méthodes d'authentification de Git
La génération d’un clé SSH se fait par la commande dans le terminal :
```
ssh-keygen -t ed25519 -C "<comment>" 
```
en mettant en commentaire mon adresse mail. Le terminal répond alors 
```
 Generating public/private ed25519 key pair. Enter file in which to save the key (/home/user/.ssh/id_ed25519):.
```
Il faut accepter et encoder une phrase d’authentification à deux reprises et le terminal confirme alors que la clé a bien été créée.
Ajout de la clé à mon compte GiltLab : J’ai copié la clé grâce à 
```
pbcopy < ~/.ssh/id_ed25519.pub. 
```
Il faut ensuite se rendre sur GitLab pour pouvoir éditer le profil. Dans l’onglet SSH Keys, il est possinle de coller ma clé dans le cadre Key, indiqué une description dans le cadre Title et terminé en cliquant que le bouton Add.

* Vérification : Pour s’assurer que la clé a bien été ajoutée, il faut entrer la commande suivante dans le terminal
```
 ssh -T git@gitlab.com 
```
Le terminal demande alors si je veux continuer la connection, accepter via "yes" et GitLab souhaite la bienvenue!

## Language Markdown
Plusieurs commandes Markdown vont être très utiles pour la réalisation de cette documentation : 
- " ''' " va ouvrir une interface de code (comme au dessus)
- Des crochets suivis de parenthèses permettent d'afficher un lien internet avec le nom du site dans les crochets et le lien html dans les parenthèses 
- Le même code qu'au dessus mais précédés d'un point d'exclamation permet l'affichage d'une image (attention à bien l'enregistrer dans les documents de la page Git et que l'image soit sélectionnée seule, [exemple](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/Capture_d_e%CC%81cran_2022-03-01_a%CC%80_13.45.14.png))
## Insérer des images, vidéos et GIF sur GitLab 
Pour illustrer cette page WEB, il est possible de rajouter au texte des images et des vidéos. La taille du fichier est importante et ne peut pas dépasser les 150 kB de stockage tandis que les vidéos seront difficilement plus petites que 500 kB. Nous allons utilisez le logiciel [GIMP](https://www.gimp.org/downloads/) pour diminuer la qualité des images (possible avec plein d'autres applications) et j'ai preféré télécharger mes vidéos sur la plateforme YouTube pour les partager en lien.  
## Travail sur MacOS 
J'ai réalisé tout ces modules à l'aide de mon PC MacBook, certaines modifications ont du être apportées pour certains codes et beaucoup de travail à été fourni pour comprendre comment mon ordi fonctionne... 
