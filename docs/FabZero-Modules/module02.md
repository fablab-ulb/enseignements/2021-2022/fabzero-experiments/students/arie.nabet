# 2. Conception Assistée par Ordinateur

Pour ce deuxième cours, une approche via deux différents logiciels fut expliquée pour de la modélisation graphique d'objets fixés. Différents modèles ont été présentés sur 3D OpenSCAD et FreeCAD, logiciels de conception en open-source permettant la création et la modification de formes qui pourront être imprimées en trois dimensions. 
Pour télécharger ces logiciels, cliquez sur les liens suivants : 

- [3D OpenSCAD](https://openscad.org/)
- [Feuilles de commandes OpenSCAD](https://openscad.org/cheatsheet/)
- [FreeCAD](https://www.freecadweb.org/index.php) 


## Conception 3D de Flexlinks 

Afin de tester Openscad et d'approfondir nos connaissances sur les possibilités graphiques mises à disposition, il a été  demander de réaliser le design d'un Flexlink en 3D inspiré du site [Thingiverse](https://www.thingiverse.com/search?q=flexlinks&type=things&sort=relevant).

### 1. Utilités 

Un Flexlink est une structure imprimée qui peut être intégrée avec les jouets LEGO afin de créer un mécanisme souhaité en fonction de la répartition des forces. Le développement d'un flexlinks permet d'expérimenter les limites de l'imprimante 3D et de comprendre l'intérêt de ces structures. La compatibilités avec les LEGO implique de concevoir avec des paramètres correspondants prédéterminés. 

### 2. Compatibilités avec les LEGO

La taille des socles et la distance entre les socles devant accueillir les trous des Flexlinks sont identiques pour tout les LEGO. On retrouve ces dimensions sur le site de [tipsandbricks](https://www.tipsandbricks.co.uk/post/1418-tips-lego-dimensions-and-units). 

- Dimensions uniques des LEGO : 
![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/Capture_d_e%CC%81cran_2022-03-01_a%CC%80_13.45.14.png)

### 3. Codes personnels 

```
/*
     FILE   : flexstick2.scad

     AUTHOR : Arié Nabet <arie.nabet@ulb.be>

     DATE   : 19-04-2022

     LICENSE : Creative Commons 4.0 [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).

     CONTRIBUTORS :
     - Code entirely written by Arié Nabet, basing on the flexlink protocol from the Fablab ULB :https://www.thingiverse.com/thing:3016939
 //*/

$fn = 150; //défini la précision des courbes

// paramètres du Flexstick à tige cylindrique : 

radius = 2.45; //rayon des trous
height = 9.6; // Hauteur de la pièce
distance = 8; // Distance centre à centre entre deux trous
thickness = 0.7; // Distance entre l'extérieur du trou et l'extérieur de la pièce
size_flex = 50; // longueur de la tige cylindrique 
side_flex = 1; // largeur de la tige cylindrique
N_holes=2;  //numeros de trous pour chaque partie
 lenght=N_holes*8;//longeur des deux blocs, modifiée pour permettre d'avoir une longeur proportionné de la piece.
 

// Code de la tige :

rotate([90,0,90])translate([0,0,-25])cylinder(h = size_flex, r1 = side_flex, r2 = side_flex, center = true);

//code des embouts 

module sticks(){
    difference(){
        hull(){
            cylinder(h = height, r = radius+thickness, center = true);
            translate([(N_holes-1)*distance,0,0]){cylinder(h = height, r = radius+thickness, center = true);}
            }
        for (i = [1:N_holes]){
            translate([(i-1)*distance,0,0]){
                cylinder(h = height, r = radius, center = true);
                }
            }
        }
    }
//Première attache 
   sticks();

// Deuxième attache
translate([-((N_holes-1)*distance)-(2*radius)-(2*thickness)-(size_flex-5),0,0]){
    sticks();
    }    
```
- Aperçu de la pièce réalisée : 
![](https://gitlab.com/fablab-ulb/enseignements/2021-2022/fabzero-experiments/students/arie.nabet/-/raw/main/docs/images/Capture%20d%E2%80%99e%CC%81cran%202022-03-02%20a%CC%80%2010.31.35.png)

